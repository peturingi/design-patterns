package eu.petur.design_patterns.adapter;

public final class Adapter implements Target {
	final Adaptee adaptee;

	public Adapter() {
		this.adaptee = new Adaptee();
	}

	public void request() {
		this.adaptee.concreteRequest();
	}
}

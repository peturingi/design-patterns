package eu.petur.design_patterns.adapter;

public interface Target {
	void request();
}

package eu.petur.design_patterns.composite;

public final class Leaf extends Component {

	final char symbol;

	public Leaf(final char symbol) {
		this.symbol = symbol;
	}

	@Override
	public void operation() {
		System.out.print(symbol);
	}

}

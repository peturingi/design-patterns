package eu.petur.design_patterns.composite;

import java.util.LinkedList;
import java.util.List;

public class Composite extends Component {
	private final List<Component> children;

	public Composite() {
		super();
		this.children = new LinkedList<>();
	}

	public void add(final Component component) {
		this.children.add(component);
	}

	public void remove(final Component component) {
		this.children.remove(component);
	}

	@Override
	public void operation() {
		for (final Component child : this.children) {
			child.operation();
		}
	}

}

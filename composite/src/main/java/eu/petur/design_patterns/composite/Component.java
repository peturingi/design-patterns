package eu.petur.design_patterns.composite;

public abstract class Component {
	public abstract void operation();
}

package eu.petur.design_patterns.composite;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class AppTest extends TestCase {

	public AppTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	public void testApp() {
		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		// Build the word "hello"
		final Composite hello = new Composite();
		for (final char letter : new char[] { 'H', 'e', 'l', 'l', 'o' }) {
			hello.add(new Leaf(letter));
		}

		// Build the word "world"
		final Composite world = new Composite();
		for (final char letter : new char[] { 'w', 'o', 'r', 'l', 'd', '!' }) {
			world.add(new Leaf(letter));
		}

		// Construct "Hello world!"
		final Composite sentance = new Composite();
		sentance.add(hello);
		sentance.add(new Leaf(' '));
		sentance.add(world);

		// Print "Hello world!"
		final Component paragraph = sentance;
		paragraph.operation();
		assertEquals("Hello world!", outContent.toString());

		System.setOut(null);
	}

	public void testRemove() {
		final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));

		final Composite word = new Composite();
		final Leaf h = new Leaf('h');
		final Leaf o = new Leaf('o');
		final Leaf i = new Leaf('i');
		word.add(h);
		word.add(o);
		word.remove(o);
		word.add(i);

		word.operation();
		assertEquals("hi", outContent.toString());

		System.setOut(null);
	}
}
